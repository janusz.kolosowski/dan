# Solutions for test exercises for DAN Communications.

Folder Task1 contains source files for numbers storage/sorting exercise.

**BstHandler.cpp** contains implementation of a binary search tree algorithm 
for the given problem.

**Task1.cpp** contains code for testing the algorithm.

**Assumptions made:**

*Duplicate entries in the list of numbers are not acceptable.*

**Task description:**

    Ir dota ļoti liela skaitļu kopa (piemēram, 1 līdz 100 miljoniem skaitļu).
    Uzrakstiet programmu tik lielas skaitļu kopas uzglabāšanai, kas ietvertu
    algoritmu maksimāli ātrai ievadītā skaitļa meklēšanai, pievienošanai un
    dzēšanai.


Folder Task2 contains source files for LED driver development exercise.

**Assumptions made:**

*Only blink and solid lit functions are necessary to be implemented by driver*

*PWM managed brightness control is not required*

*GPIO pin export/unexport to allow user access without root privileges is not required*

*A user function will reliably call the driver main task function within prescribed time interval*

**Task description:**

    Kādā iekārtā (piemēram, Raspberry Pi) ir doti porti (piemēram, GPIO).
    Pie šiem portiem ir pieslēgtas gaismas diodes. Uzrakstiet draiveri 
    šai iekārtai, kas ļautu kontrolēt šādas gaismas diodes. 
