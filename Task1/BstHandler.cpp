
#include "BstHandler.h"
#include <iostream>

namespace Task1
{
	//------------------------
	// BinarySearchTree class
	// Private members:
	//------------------------

	// Recursive function to be used for adding
	// new keys to a binary search tree.
	BstNode* BinarySearchTree::Insert(BstNode* node, int newKey)
	{
		// Check if we reached the end of a tree
		if (node == NULL)
		{
			node = new BstNode();
			node->Key = newKey;
			node->Left = NULL;
			node->Right = NULL;
#if _DEBUG
			printf("Number %d successfully added to BST.\n", newKey);
#endif
		}
		// Check if the new key is larger than current node's key.
		else if (newKey > node->Key)
		{
			node->Right = Insert(node->Right, newKey);
			node->Right->Parent = node;
		}
		// Check if the new key is smaller than current node's key.
		else if (newKey < node->Key)
		{
			node->Left = Insert(node->Left, newKey);
			node->Left->Parent = node;
		}
		// Check if the new key already exists in BST.
		else
		{
#if _DEBUG
			printf("Can't add number %d, it already exists in BST.\n", newKey);
#endif
		}
		return node;
	}


	// Recursive function to be used for searching
	// for specified key within a binary search tree.
	BstNode* BinarySearchTree::Search(BstNode* node, int searchKey)
	{
		// Return null if key is not found.
		if (node == NULL)
		{
			return NULL;
		}
		else if (searchKey == node->Key)
		{
			return node;
		}
		// Keep searching right side of tree if searchKey is greater than current node's key
		else if (searchKey > node->Key)
		{
			return Search(node->Right, searchKey);
		}
		// Keep searching left side of tree if searchKey is smaller than current node's key
		else
		{
			return Search(node->Left, searchKey);
		}
	}

	int BinarySearchTree::Min(BstNode* node)
	{
		// Examining an end node - nothing to do.
		if (node == NULL)
		{
			return -1;
		}
		// 
		else if (node->Left == NULL)
		{
			return node->Key;
		}
		else
		{
			return Min(node->Left);
		}
	}

	// Searches for smallest number larger than specified node's key
	int BinarySearchTree::Next(BstNode* node)
	{
		// if node has a right subtree, search the subtree for next value.
		if (node->Right != NULL)
		{
			return Min(node->Right);
		}
		// if node has no right subtree, search the parents up leftwards.
		BstNode* parentNode = node->Parent;
		BstNode* currentNode = node;
		while (parentNode != NULL
			&& currentNode == parentNode->Right)
		{
			currentNode = parentNode;
			parentNode = currentNode->Parent;
		}
		if (parentNode != NULL)
		{
			return parentNode->Key;
		}
		else return -1;
	}
	
	int BinarySearchTree::Next(int searchKey)
	{
		BstNode* searchNode = Search(Root, searchKey);
		if (searchNode == NULL)
		{
			printf("Couldn't find a corresponding node for specified key %d.\n", searchKey);
			return -1;
		}
		else
		{
			return Next(searchNode);
		}
	}


	//------------------------
	// BinarySearchTree class
	// Public members:
	//------------------------

	void BinarySearchTree::Insert(int newKey)
	{
		Root = Insert(Root, newKey);
	}

	bool BinarySearchTree::Search(int searchKey)
	{
		BstNode* result = Search(Root, searchKey);
		if (result == NULL)
		{
#if _DEBUG
			printf("Number %d not found in BST by search operation.\n", searchKey);
#endif
			return false;
		}
		else
		{
#if _DEBUG
			printf("Number %d successfully found in BST.\n", searchKey);
#endif
			return true;
		}
	}

	// Recursive function for removing a BST node from the tree
	// based on input key.
	BstNode* BinarySearchTree::Remove(BstNode* node, int oldKey)
	{
		// Check if the given node exists
		if (node == NULL)
		{
			printf("Can't remove number %d from BST, number not found.\n", oldKey);
			return NULL;
		}
		// Handle 4 different cases when a node for removal is found
		else if (node->Key == oldKey)
		{
			// Case 1 - node has no children.
			if (node->Left == NULL && node->Right == NULL)
			{
				node = NULL;
				printf("Number %d with no children successfully removed from BST.\n", oldKey);
			}
			// Case 2 - node has a left child.
			else if (node->Left != NULL && node->Right == NULL)
			{
				node->Left->Parent = node->Parent;
				node = node->Left;
				printf("Number %d with left child %d successfully removed from BST.\n", oldKey, node->Key);
			}
			// Case 3 - node has a right child.
			else if (node->Left == NULL && node->Right != NULL)
			{
				node->Right->Parent = node->Parent;
				node = node->Right;
				printf("Number %d with right child %d successfully removed from BST.\n", oldKey, node->Key);
			}
			// Case 4 node has left and right children
			else
			{
				// Find next ascending node to replace current node
				int nextKey = Next(oldKey);
				// Remove found ascending node
				node->Right = Remove(node->Right, nextKey);
			}
		}
		// Keep searching for correct node in case keys don't match
		else if (oldKey > node->Key)
		{
			node->Right = Remove(node->Right, oldKey);
		}
		else
		{
			node->Left = Remove(node->Left, oldKey);
		}
		return node;
	}
	void BinarySearchTree::Remove(int oldKey)
	{
		Root = Remove(Root, oldKey);
	}
}
