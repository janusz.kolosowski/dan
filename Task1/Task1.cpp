// Task1.cpp : This file contains code for testing an implementation of a binary search tree, which is the formal solution to the requested exercise.
#include "BstHandler.h"
#include <cstdio>
#include <iostream>
#include <math.h>

int main()
{
	printf("Start of program execution.\n");
	Task1::BinarySearchTree* tree = new Task1::BinarySearchTree();
	
	// Test of Insert()
	// Insert root
	tree->Insert(50);
	// Insert duplicate root
	tree->Insert(50);
	// Insert new node higher than root
	tree->Insert(55);
	// Insert a new node inbetween existing nodes
	tree->Insert(53);
	// Insert a new node lower than root
	tree->Insert(40);
	// Insert a new maximum node
	tree->Insert(57);
	// Insert a new minimum node
	tree->Insert(30);
	// Insert an already existing node
	tree->Insert(57);
	
	// Test of Search()
	// Search for root
	tree->Search(50);
	// Search for in-between node
	tree->Search(53);
	// Search for non-existent node
	tree->Search(20);
	// Search for maximum node
	tree->Search(57);
	// Search for minumum node
	tree->Search(30);

	// Test of Remove()
	// Remove root
	tree->Remove(50);
	// Remove root again
	tree->Remove(50);
	// Remove intermediate node with left child
	tree->Remove(40);
	// Remove intermediate node with right child
	tree->Remove(55);
	// Remove maximum node
	tree->Remove(57);
	// Remove minimum node
	tree->Remove(30);

	return std::getchar();
}
