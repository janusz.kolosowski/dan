#ifndef NUMBER_ARRAY_H
#define NUMBER_ARRAY_H
namespace Task1
{
	class BstNode
	{
	public:
		int Key;
		BstNode* Left;
		BstNode* Right;
		BstNode* Parent;
	};

	class BinarySearchTree
	{
	private:
		BstNode* Root;
		BstNode* Insert(BstNode* node, int newKey);
		BstNode* Search(BstNode* node, int searchKey);
		BstNode* Remove(BstNode* node, int oldKey);
		int Min(BstNode* node);
		int Next(BstNode* node);
		int Next(int searchKey);
	public:
		void Insert(int newKey);
		bool Search(int searchKey);
		void Remove(int oldKey);
	};
}

#endif


