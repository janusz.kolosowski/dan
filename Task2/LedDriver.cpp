// Task2.cpp : Simple driver for controlling a GPIO connected LED.

#include "LedDriver.h"
#include <chrono>
#include <cmath>
#include <iostream>
#include <thread>
#define gpio(x) (*(volatile uint32_t *)(x))
#define FSEL_REGISTER (0x7E200000)
#define OUT_SET_REGISTER (0x7E20001C)
#define OUT_CLEAR_REGISTER (0x7E200028)

typedef struct LockType
{
	uint8_t lockId;
	bool isLocked;
	bool isLockedBy(uint8_t id)
	{
		return isLocked && (lockId == id);
	}
} LockType;

LedDriver::LedMode currentMode = LedDriver::LEDMODE_DISABLED;
uint16_t dutyCyclePeriodMs = 0;
uint16_t blinkPeriodMs = 1000;
auto lastUpdateTime = std::chrono::system_clock::now();
LockType lock = LockType();

// Private functions:

void LedDriver::startModule()
{
	// set the GPIO operating mode of a prespecified GPIO pin to "output"
	// this would be done by writing the corresponding pin mode to
	// the GPIO register.

	// Some pseudo-code for illustration using data from
	// https://www.raspberrypi.org/app/uploads/2012/02/BCM2835-ARM-Peripherals.pdf
	gpio(FSEL_REGISTER) &= 0xFFFFFFFC;
	// Note: Management of system access rights is not covered in this solution.
}

void LedDriver::stopModule()
{
	// set the GPIO operating mode of a prespecified GPIO pin back to "input"
	// this would be done by writing the corresponding pin mode to
	// the GPIO register.
	// Some pseudo-code for illustration using data from
	// https://www.raspberrypi.org/app/uploads/2012/02/BCM2835-ARM-Peripherals.pdf
	gpio(FSEL_REGISTER) &= 0xFFFFFFFB;
}

void LedDriver::setGpioOutput(GpioOutput value)
{
	// write the corresponding 0 or 1 value to a predetermined GPIO port
	// Some pseudo-code for illustration using data from
	// https://www.raspberrypi.org/app/uploads/2012/02/BCM2835-ARM-Peripherals.pdf
	gpio(OUT_SET_REGISTER) &= (0xFFFFFFFE + value);
	gpio(OUT_CLEAR_REGISTER) &= (0xFFFFFFFE + !value);
}

// Public functions:

void LedDriver::requestLock(uint8_t id)
{
	if (lock.isLocked == false)
	{
		lock.isLocked = true;
		lock.lockId = id;
	}
}
void LedDriver::releaseLock(uint8_t id)
{
	if (lock.isLockedBy(id))
	{
		lock.isLocked == false;
	}
}

// Allows external programs to modify LED duty cycle length for
// LEDMODE_BLINK pattern
int LedDriver::setDutyCyclePeriodMs(uint8_t id, uint16_t periodMs)
{
	if (lock.isLocked == false || lock.isLockedBy(id))
	{
		dutyCyclePeriodMs = periodMs;
		return 0;
	}
	else
	{
		return - 1;
	}
}

// Allows external programs to modify LED blink period length for
// LEDMODE_BLINK pattern
int LedDriver::setBlinkPeriodMs(uint8_t id, uint16_t periodMs)
{
	if (lock.isLocked == false || lock.isLockedBy(id))
	{
		blinkPeriodMs = periodMs;
		return 0;
	}
	else
	{
		return -1;
	}
}

// Allows external programs to modify LED pattern and
// enable/disable driver.
int LedDriver::setMode(uint8_t id, LedMode mode)
{
	if (lock.isLocked == false || lock.isLockedBy(id))
	{
		if (mode != LEDMODE_DISABLED && currentMode == LEDMODE_DISABLED)
		{
			startModule();
		}
		currentMode = mode;
	}
	else
	{
		return -1;
	}
}

void LedDriver::LedCyclic10Ms()
{
	bool isRunning = true;
	uint16_t blinkPeriodCounterMs = 0;
	uint16_t dutyPeriodCounterMs = 0;

	// Start the main task of the driver.
	switch (currentMode)
	{
	case LEDMODE_DISABLED:
		stopModule();
		isRunning = false;
		break;
	case LEDMODE_OFF:
		setGpioOutput(GPIOOUTPUT_OFF);
		break;
	case LEDMODE_SOLID:
		setGpioOutput(GPIOOUTPUT_ON);
		break;
	case LEDMODE_BLINK:
		// Check if our operation falls within duty cylce
		// If yes, drive LED as usual
		if (dutyPeriodCounterMs < dutyCyclePeriodMs)
		{
			setGpioOutput(GPIOOUTPUT_OFF);
		}
		// If the duty cycle for this blink period is already over, turn LED off.
		else
		{
			setGpioOutput(GPIOOUTPUT_OFF);
		}
		break;
	}
	// Increment and reset time counters where necessary.
	// Use system time to determine how much time has passed since last call
	// of LedCyclic10Ms().
	int timeDeltaMs = std::chrono::duration_cast<std::chrono::milliseconds>
					 (std::chrono::system_clock::now() - lastUpdateTime).count;

	blinkPeriodCounterMs += timeDeltaMs;
	dutyPeriodCounterMs += timeDeltaMs;

	if (blinkPeriodCounterMs >= blinkPeriodMs)
	{
		dutyPeriodCounterMs = 0;
		blinkPeriodCounterMs = 0;
	}

	lastUpdateTime = std::chrono::system_clock::now();
}




