#ifndef LED_DRIVER_H
#define LED_DRIVER_H

#include <cstdint>

static class LedDriver
{
private:
	typedef enum GpioOutput
	{
		GPIOOUTPUT_OFF,
		GPIOOUTPUT_ON
	} GpioOutput;

	void startModule();
	void stopModule(void);

	void setGpioOutput(GpioOutput value);
public:
	typedef enum LedMode
	{
		LEDMODE_DISABLED,
		LEDMODE_OFF,
		LEDMODE_SOLID,
		LEDMODE_BLINK
	} LedMode;

	void requestLock(uint8_t id);
	void releaseLock(uint8_t id);
	int setDutyCyclePeriodMs(uint8_t id, uint16_t periodMs);
	int setBlinkPeriodMs(uint8_t id, uint16_t periodMs);
	int setMode(uint8_t id, LedMode mode);
	void LedCyclic10Ms(void);
};
#endif
